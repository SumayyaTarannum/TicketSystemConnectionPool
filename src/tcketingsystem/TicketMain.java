package tcketingsystem;

import java.util.Calendar;
import java.util.List;
import java.util.Scanner;
import java.util.TreeSet;

public class TicketMain {

	@SuppressWarnings("resource")
	public static void main(String args[]) throws Exception {
		System.out.println("--------------------------------------------------");
		System.out.println("            WELCOME TO TICKETING SYSTEM           ");
		System.out.println("--------------------------------------------------");
		TicketSource store = TicketSource.getInstance();
		int input = 0;
		int count = 0;
		Scanner scanner = new Scanner(System.in);

		do {
			System.out.println(
					"1.Create Ticket\n2.Update Ticket\n3.Search Ticket by Title\n4.Search Ticket by Unique Id\n"
							+ "5.Search Ticket by Status\n6.Search Ticket by Category\n7.Get Recently Added Ticket\n8.Exit");
			System.out.println("Enter Your Choice:");
			input = scanner.nextInt();
			scanner.nextLine();
			switch (input) {
			case 1:
				String description;
				String raisedBy;
				System.out.println("Please Enter Title::");
				String title = scanner.nextLine();
				System.out.println("Please Enter Category:WATER,NETWORK,CHAIRS,TABLE,SPACE,COFFEE_TEA,CUPS");
				String category = scanner.nextLine();
				for (Category c : Category.values()) {
					if (c.name().equals(category)) {
						System.out.println("Please Enter Description::");
						description = scanner.nextLine();
						System.out.println("Please Enter Name::");
						raisedBy = scanner.nextLine();
						Ticket ticket=store.addTicket(title, Category.valueOf(category), description, raisedBy);
						System.out.println("Ticket UniqueId     :" + ticket.getUniqueId());
						System.out.println("Ticket Title        :" + title);
						System.out.println("Ticket CustomerName :" + raisedBy);
						System.out.println("Ticket Description  :" + description);
						System.out.println("Ticket Category     :" + category);
						System.out.println("Ticket Status       :" + TicketStatus.NEW);						
						TicketAction ticketAction = new TicketAction();
						System.out.println("Ticket Action TakenBY:" + ticketAction.getActionTakenBy("Shilpa"));
						System.out.println("Ticket Creation Time:" + Calendar.getInstance().getTime());
						System.out.println("--------------------------------------------------");
					} else {
						count++;
					}
				}
				if (count == 7) {
					System.out.println();
				}
				break;
			case 2:
				String description1;
				String raisedBy1;
				int enteredId = 0;
				try {
					System.out.println("Please Enter Your UniqueId :");
					enteredId = scanner.nextInt();
					scanner.nextLine();
					System.out.println("Please Enter Title::");
					String title1 = scanner.nextLine();
					System.out.println("Please Enter Category:WATER,NETWORK,CHAIRS,TABLE,SPACE,COFFEE_TEA,CUPS");
					String category1 = scanner.nextLine();
					for (Category c : Category.values()) {
						if (c.name().equals(category1)) {
							System.out.println("Please Enter Description::");
							description1 = scanner.nextLine();
							System.out.println("Please Enter Name::");
							raisedBy1 = scanner.nextLine();
							store.updateTicket(enteredId, title1, Category.valueOf(category1), description1, raisedBy1,
									TicketStatus.IN_PROGRESS);
							System.out.println("--------------------------------------------------");
							System.out.println("Ticket Title        :" + title1);
							System.out.println("Ticket CustomerName :" + raisedBy1);
							System.out.println("Ticket Description  :" + description1);
							System.out.println("Ticket Category     :" + category1);
							System.out.println("Ticket Staus        :" + TicketStatus.IN_PROGRESS);
							System.out.println("Ticket Creation Time:" + Calendar.getInstance().getTime());
							System.out.println("--------------------------------------------------");
						} else {
							count++;
						}
					}
					if (count == 7) {
						System.out.println();
					}
				} catch (Exception e) {
					System.out.println("Invalid UniqueID");
				}
				break;
			case 3:
				try {
					System.out.println("Enter Ticket Title:");
					String ticketTitle = scanner.nextLine();
					TreeSet<Ticket> ticketListTitle = store.getTicketByTitle(ticketTitle);
					for (Ticket ticket2 : ticketListTitle) {
						System.out.println("Ticket Id           :" + ticket2.getUniqueId());
						System.out.println("Ticket Title        :" + ticket2.getTitle());
						System.out.println("Ticket CustomerName :" + ticket2.getTicketRaisedBy());
						System.out.println("Ticket Category     :" + ticket2.getCategory());
						System.out.println("Ticket Description  :" + ticket2.getDescription());
						System.out.println("Ticket Status       :" + ticket2.getTicketStatus());
						System.out.println("Ticket Creation Time:" + ticket2.getTicketCreationTime());
						System.out.println("--------------------------------------------------");
					}
				} catch (Exception e) {
					System.out.println("Ticket Doesn't Exist with this Title");
				}
				break;
			case 4:
				try {
					System.out.println("Please Enter Your UniqueID");
					int uniqueId = scanner.nextInt();
					scanner.nextLine();
					TreeSet<Ticket> ticketUniqueId = store.getByUniqueId(uniqueId);
					for (Ticket ticket2 : ticketUniqueId) {
						System.out.println("Ticket Id           :" + ticket2.getUniqueId());
						System.out.println("Ticket Title        :" + ticket2.getTitle());
						System.out.println("Ticket CustomerName :" + ticket2.getTicketRaisedBy());
						System.out.println("Ticket Category     :" + ticket2.getCategory());
						System.out.println("Ticket Description  :" + ticket2.getDescription());
						System.out.println("Ticket Status       :" + ticket2.getTicketStatus());
						System.out.println("Ticket Creation Time:" + ticket2.getTicketCreationTime());
						System.out.println("--------------------------------------------------");
					}

				} catch (NullPointerException e) {
					System.out.println("Ticket Doesn't Exist with this UniqueID");
				}
				break;
			case 5:
				try {
					System.out.println("Enter Ticket Status");
					String ticketStatus = scanner.nextLine();
					TreeSet<Ticket> ticketList =  store.getTicketByStatus(TicketStatus.valueOf(ticketStatus));
					for (Ticket ticket2 : ticketList) {
						System.out.println("Ticket Id           :" + ticket2.getUniqueId());
						System.out.println("Ticket Title        :" + ticket2.getTitle());
						System.out.println("Ticket CustomerName :" + ticket2.getTicketRaisedBy());
						System.out.println("Ticket Category     :" + ticket2.getCategory());
						System.out.println("Ticket Description  :" + ticket2.getDescription());
						System.out.println("Ticket Status       :" + ticket2.getTicketStatus());
						System.out.println("Ticket Creation Time:" + ticket2.getTicketCreationTime());
						System.out.println("--------------------------------------------------");
					}
				} catch (Exception e) {
					System.out.println("Ticket Doesn't Exist with this Status");
				}
				break;
			case 6:
				try {
					System.out.println("Please enter your Category");
					String ticketCategory = scanner.nextLine();
					TreeSet<Ticket> ticketListCategory = store.getTicketByCategory(Category.valueOf(ticketCategory));
					for (Ticket ticket2 : ticketListCategory) {
						System.out.println("Ticket Id           :" + ticket2.getUniqueId());
						System.out.println("Ticket Title        :" + ticket2.getTitle());
						System.out.println("Ticket CustomerName :" + ticket2.getTicketRaisedBy());
						System.out.println("Ticket Category     :" + ticket2.getCategory());
						System.out.println("Ticket Description  :" + ticket2.getDescription());
						System.out.println("Ticket Status       :" + ticket2.getTicketStatus());
						System.out.println("Ticket Creation Time:" + ticket2.getTicketCreationTime());
						System.out.println("--------------------------------------------------");
					}
				} catch (Exception e) {
					System.out.println("Ticket Doesn't Exist with this Category");
				}
				break;
			case 7:
				System.out.println("Recently Added ticket is");
				TreeSet<Ticket> ticketListCategory = store.getMostRecentTicket();
				for (Ticket ticket2 : ticketListCategory) {
					System.out.println("Ticket Id           :" + ticket2.getUniqueId());
					System.out.println("Ticket Title        :" + ticket2.getTitle());
					System.out.println("Ticket CustomerName :" + ticket2.getTicketRaisedBy());
					System.out.println("Ticket Category     :" + ticket2.getCategory());
					System.out.println("Ticket Description  :" + ticket2.getDescription());
					System.out.println("Ticket Status       :" + ticket2.getTicketStatus());
					System.out.println("Ticket Creation Time:" + ticket2.getTicketCreationTime());
					System.out.println("--------------------------------------------------");
				}
				break;
			case 8:
				System.out.println("Thank You");
				break;
			default:
				System.err.println("WRONG CHOICE");
				break;
			}
		} while (input != 8);

	}
}
