package tcketingsystem;

import java.util.Date;
import java.util.List;

public class Ticket implements Comparable<Ticket> {

	public static int count = 1;
	private int uniqueId; // unique
	private String title;
	private Category category;
	private String description;
	private Date ticketCreationTime;
	private TicketStatus ticketStatus;
	private String ticketRaisedBy;
	private List<TicketAction> actions;

	public int getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(int uniqueId) {
		this.uniqueId = uniqueId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getTicketCreationTime() {
		return ticketCreationTime;
	}

	public void setTicketCreationTime(Date ticketCreationTime) {
		this.ticketCreationTime = ticketCreationTime;
	}

	public TicketStatus getTicketStatus() {
		return ticketStatus;
	}

	public void setTicketStatus(TicketStatus ticketStatus) {
		this.ticketStatus = ticketStatus;
	}

	public String getTicketRaisedBy() {
		return ticketRaisedBy;
	}

	public void setTicketRaisedBy(String ticketRaisedBy) {
		this.ticketRaisedBy = ticketRaisedBy;
	}

	public List<TicketAction> getActions() {
		return actions;
	}

	public void setActions(List<TicketAction> actions) {
		this.actions = actions;
	}

	@Override
	public int compareTo(Ticket ticket) {
				return ticketCreationTime.compareTo(ticket.ticketCreationTime);
	}	
}