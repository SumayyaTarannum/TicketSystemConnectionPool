package tcketingsystem;

public enum TicketStatus {
	NEW, IN_PROGRESS, REJECTED, RESOLVED
}
