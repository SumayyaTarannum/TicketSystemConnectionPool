package tcketingsystem;

public enum Category {
	WATER, NETWORK, CHAIRS, TABLE, SPACE, COFFEE_TEA, CUPS
}
