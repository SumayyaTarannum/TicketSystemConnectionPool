package tcketingsystem;

public class TicketAction {

	@SuppressWarnings("unused")
	private String actionTakenBy;
	private String comments;

	public String getActionTakenBy(String actionTakenBy) {
		return actionTakenBy;
	}

	public void setActionTakenBy(String actionTakenBy) {
		this.actionTakenBy = actionTakenBy;

	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

}
